#!/bin/sh

API="https://api.kraken.com/0/public/Ticker"

# Last Trade closed is result c.
quote=$(curl -sf $API?pair=ETHUSDT | jq -r ".result.ETHUSDT.c[0]")
quote=$(LANG=C printf "%.2f" "$quote")

echo "Ξ $quote ₮"
