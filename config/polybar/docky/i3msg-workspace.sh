#!/usr/bin/env bash
# Written by Martin Zeller

WORKSPACES=$(i3-msg -t get_workspaces)
CURRENT_WS=$(echo "$WORKSPACES" | jq ".[]|select(.focused == true)")
OUTPUT=$(echo "$CURRENT_WS" | jq ".output")
OUTPUT_WSS=$(echo "$WORKSPACES" | jq ".[]|select(.output == $OUTPUT)")
INDEX=$(echo "$OUTPUT_WSS" | jq -s "map(.focused)|index(true)")
LENGTH=$(echo "$OUTPUT_WSS" | jq -s "length")

if [[ $1 -gt 0 ]]; then
	# Forwards
	if (( $INDEX + 1 < $LENGTH )); then
	    i3-msg workspace next_on_output
	fi
elif [[ $1 -lt 0 ]]; then
	# Backwards
	echo $INDEX
	if (( $INDEX - 1 >= 0 )); then
	    i3-msg workspace prev_on_output
	fi
fi
