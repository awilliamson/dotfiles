#!/bin/sh

if [[ ! -z $(exec cat /proc/net/route | grep tun0) ]]
then
	ip_tun0=$(ip addr show tun0 | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
	echo "%{F#339933} $ip_tun0 %{F-}"
else
	echo "%{F#993333}DOWN%{F-}"
fi

#printf "VPN: " && (pgrep -a openvpn$ | head -n 1 | awk '{print $NF }' | cut -d '.' -f 1 && echo down) | head -n 1

#exec cat /proc/net/route | grep tun0
