#!/bin/bash
FILES="bashrc Xprofile Xresources bin i3 config/nvim config/fish config/compton.conf config/polybar config/alacritty"

mkdir -p ~/dotfiles_old
cd ~/dotfiles

for FILE in $FILES; do
	if [ -e ~/.$FILE ]; then
		mv ~/.$FILE ~/dotfiles_old
	fi
	ln -s ~/dotfiles/$FILE ~/.$FILE
done

cp ~/dotfiles/xinitrc /etc/X11/xinit/xinitrc

git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.st status
